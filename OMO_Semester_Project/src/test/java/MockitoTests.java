import cz.cvut.fel.omo.foodChain.chainParticipants.*;
import cz.cvut.fel.omo.foodChain.operations.*;
import cz.cvut.fel.omo.foodChain.channels.*;
import cz.cvut.fel.omo.foodChain.product.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static cz.cvut.fel.omo.foodChain.product.FoodEnum.*;
import static cz.cvut.fel.omo.foodChain.product.FoodEnum.WATER;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

public class MockitoTests {

    private MeatFarmer meatFarmer;
    private VegetableFarmer vegetableFarmer;
    private Manufacturer manufacturer;
    private Storehouse storehouse;
    private Seller seller;
    private Customer customer;
    private TransactionInformer transactionInformer;
    private List<FoodEnum> meatIngredients = Arrays.asList(MILK, MEAT, BUTTER, EGGS);
    private List<FoodEnum> vegetableIngredients = Arrays.asList(POTATO, ONION, SUGAR, FLOUR, BEET, WATER);
    private List<FoodEnum> readyMeals = Arrays.asList(PANCAKES, KYIVCUTLET, DRANIKI, ICECREAM, BORSHCH);

    @BeforeEach
    void setUp() {
        this.meatFarmer = new MeatFarmer("MEAT FARMER", 35, "BELARUS", 2000,
                this.meatIngredients, this.transactionInformer);
        this.vegetableFarmer = new VegetableFarmer("VEGETABLE FARMER", 42, "UKRAINE", 2000,
                this.vegetableIngredients, this.transactionInformer);
        this.manufacturer = new Manufacturer("BORSHCH MANUFACTURER", 1000,
                BORSHCH, this.transactionInformer);
        this.storehouse = new Storehouse("STOREHOUSE", 1000, this.transactionInformer);
        this.seller = new Seller("SELLER", 1000, this.transactionInformer);
        this.customer = new Customer("CUSTOMER", 4000, this.transactionInformer);
        this.transactionInformer = new TransactionInformer();
    }

    void setUpChannels(List<Party> participants) {
        MeatChannel.getMeatChannel(participants, meatIngredients, transactionInformer);
        VegetableChannel.getVegetableChannel(participants, vegetableIngredients, transactionInformer);
        ReadyMealChannel.getReadyMealChannel(participants, readyMeals, transactionInformer);
        PaymentChannel.getPaymentChannel(participants, transactionInformer);

        for (Party participant : participants) {
            transactionInformer.attach(participant);
            participant.registerToTheChannel();
        }
    }

    @Test
    void addFood_VegetableChannelExecutesRequest_ApplicantGetsFood() {

        // arrange
        Manufacturer manufacturer = mock(Manufacturer.class);
        List<Party> participants = new ArrayList<>();
        Collections.addAll(participants, manufacturer, this.vegetableFarmer);
        setUpChannels(participants);
        Request request = new Request(manufacturer, POTATO, 1, null);
        FoodEntity foodEntity = new FoodEntity(POTATO, 1, 10);

        //act
        VegetableChannel.getVegetableChannel().setApplicant(manufacturer);
        VegetableChannel.getVegetableChannel().setExecutor(this.vegetableFarmer);
        VegetableChannel.getVegetableChannel().setFoodEntity(foodEntity);
        VegetableChannel.getVegetableChannel().executeRequest(request);

        // assert
        verify(manufacturer, times(1)).addFood(foodEntity);
    }

    @Test
    void getProfit_SellerPaysFarmer50_FarmerHasToGetMoney() {

        // arrange
        VegetableFarmer farmer = spy(this.vegetableFarmer);
        List<Party> participants = new ArrayList<>();
        Collections.addAll(participants, farmer, this.seller);
        setUpChannels(participants);
        int initialBalance = farmer.getCashAccount();
        int expectedBalance = initialBalance + 50;

        // act
        PaymentChannel.getPaymentChannel().makePayment(this.seller, farmer, 50);

        // assert
        verify(farmer, times(1)).getProfit();
        assertEquals(farmer.getCashAccount(), expectedBalance);
    }

    @Test
    void notify_NewTransactionBetweenStoreHouseAndMeatFarmerWasExecuted_VegetableFarmerGetsInformation() {

        // arrange
        VegetableFarmer vegetableFarmer = mock(VegetableFarmer.class);
        Transaction transaction = new Transaction(new Request(this.storehouse, MEAT, 1, null),
                this.meatFarmer, 0);
        List<Party> participants = new ArrayList<>();
        Collections.addAll(participants, vegetableFarmer, this.meatFarmer, this.storehouse);
        setUpChannels(participants);

        // act
        this.transactionInformer.notify(transaction);

        // assert
        verify(vegetableFarmer, times(1)).updateTransactions(transaction);
    }

    @Test
    void process_CustomerSendsRequestToReceiveMeat_SellerShouldProcessRequest() {

        // arrange
        Seller seller = spy(this.seller);
        List<Party> participants = new ArrayList<>();
        Collections.addAll(participants, seller, this.customer);
        setUpChannels(participants);
        seller.addFood(new FoodEntity(MEAT, 1, 10));

        // act
        this.customer.sendRequest(MEAT, 1);

        // assert
        verify(seller, times(1)).process(any(Request.class));
        assertEquals(this.customer.getProducts().size(), 1);
    }

    @Test
    void process_ManufacturerWantsToBuyBorshch_FarmersMustProduceOnlyNeededIngredients() {

        // arrange
        List<Party> participants = new ArrayList<>();
        MeatFarmer meatFarmer = spy(this.meatFarmer);
        VegetableFarmer vegetableFarmer = spy(this.vegetableFarmer);
        Collections.addAll(participants, vegetableFarmer, meatFarmer, this.manufacturer);
        setUpChannels(participants);

        // act
        this.manufacturer.sendRequest(BORSHCH, 1);

        // assert
        verify(meatFarmer, times(1)).process(any(Request.class));
        verify(vegetableFarmer, times(3)).process(any(Request.class));
    }
}
