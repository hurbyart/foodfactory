package cz.cvut.fel.omo.foodChain.channels;

import cz.cvut.fel.omo.foodChain.product.FoodEnum;
import cz.cvut.fel.omo.foodChain.chainParticipants.*;
import cz.cvut.fel.omo.foodChain.channels.MeatChannel;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static cz.cvut.fel.omo.foodChain.product.FoodEnum.*;
import static cz.cvut.fel.omo.foodChain.product.FoodEnum.BORSHCH;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class MeatChannelTest {


    //TODO perechekat ves test ego ne smotrel

    private Party meatFarmer;
    private Party manufacturer;
    private Party customer;
    private List<FoodEnum> meatIngredients = new ArrayList<>();
    private List<Party> participants = new ArrayList<>();

    @BeforeEach
    void setUp() {
        Collections.addAll(meatIngredients, MILK, MEAT, BUTTER, EGGS);
        meatFarmer = new MeatFarmer("Tomas",42, "USA", 1000, meatIngredients, null);
        manufacturer = new Manufacturer("Tomas", 1000, BORSHCH, null);
        customer = new Customer("Julie", 1000, null);
        Collections.addAll(participants, meatFarmer, manufacturer, customer);
        MeatChannel.getMeatChannel(participants, meatIngredients, null);
        MeatChannel.getMeatChannel().register(meatFarmer);
        MeatChannel.getMeatChannel().register(manufacturer);
        MeatChannel.getMeatChannel().register(customer);
    }

    @Test
    void registerParticipantsToMeatChannel_meatFarmerManufacturer_allWasRegistered() {
        // arrange
        List<Party> expectedParticipants = new ArrayList<>();
        Collections.addAll(expectedParticipants, meatFarmer, manufacturer, customer);
        int expectedSize = 3;

        // act
        int actualSize = MeatChannel.getMeatChannel().getCurrentParticipants().size();

        // assert
        assertEquals(expectedSize, actualSize);
    }
}