package cz.cvut.fel.omo.foodChain.chainParticipants;

import cz.cvut.fel.omo.foodChain.product.FoodEntity;
import cz.cvut.fel.omo.foodChain.product.FoodEnum;
import cz.cvut.fel.omo.foodChain.chainParticipants.Seller;
import cz.cvut.fel.omo.foodChain.operations.OperationEnum;
import cz.cvut.fel.omo.foodChain.operations.Request;
import cz.cvut.fel.omo.foodChain.visitor.PriceWriter;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;


public class SellerTest {

    private Seller seller;
    private Request request;

    @BeforeEach
    void setUp() {
        seller = new Seller("seller", 500, null);
        request = new Request(null, FoodEnum.MILK, 1, OperationEnum.SELL);
    }

    @Test
    void askPrice_kyivCutletPrice_3000() {
        // arrange
        int expectedPrice = 3000;
        seller.accept(new PriceWriter());
        // act
        int actualPrice = seller.askPrice(FoodEnum.KYIVCUTLET, 10);
        // assert
        assertEquals(expectedPrice, actualPrice);
    }

    @Test
    void IsAgreeToExecute_productWhichSellerHas_agreeToExecute() {
        // arrange
        seller.addFood(new FoodEntity(FoodEnum.MILK, 1, 5));
        // act
        boolean agreeToExecute = seller.isAgreeToExecute(request);
        // assert
        assertTrue(agreeToExecute);
    }

    @Test
    void IsAgreeToExecute_productWhichSellerDoesntHave_notAgreeToExecute() {
        // act
        boolean agreeToExecute  = seller.isAgreeToExecute(request);
        // assert
        assertFalse(agreeToExecute);
    }

    @Test
    void sellerProcess_milk_sellerCanProcessThisRequest() {
        // arrange
        FoodEntity expectedProduct = new FoodEntity(FoodEnum.MILK, 1, 5);
        seller.addFood(expectedProduct);
        // act
        FoodEntity actualProduct = seller.process(request);
        // assert
        assertEquals(expectedProduct, actualProduct); //fixTest
    }


}