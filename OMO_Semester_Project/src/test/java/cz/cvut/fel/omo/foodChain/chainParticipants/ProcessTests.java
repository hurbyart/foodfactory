package cz.cvut.fel.omo.foodChain.chainParticipants;

import cz.cvut.fel.omo.foodChain.channels.MeatChannel;
import cz.cvut.fel.omo.foodChain.channels.PaymentChannel;
import cz.cvut.fel.omo.foodChain.channels.ReadyMealChannel;
import cz.cvut.fel.omo.foodChain.channels.VegetableChannel;
import cz.cvut.fel.omo.foodChain.operations.TransactionInformer;
import cz.cvut.fel.omo.foodChain.product.FoodEntity;
import cz.cvut.fel.omo.foodChain.product.FoodEnum;
import cz.cvut.fel.omo.foodChain.simulation.Main;
import cz.cvut.fel.omo.foodChain.visitor.PriceWriter;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static cz.cvut.fel.omo.foodChain.product.FoodEnum.*;
import static org.junit.jupiter.api.Assertions.*;

class ProcessTests {

    List<Party> participants = new ArrayList<>();
    TransactionInformer transactionInformer = new TransactionInformer();
    List<FoodEnum> meatIngredients  = Arrays.asList(MILK, MEAT, BUTTER, EGGS);;
    List<FoodEnum> vegetableIngredients  = Arrays.asList(POTATO, ONION, SUGAR, FLOUR, BEET, WATER);;
    List<FoodEnum> readyMeals  = Arrays.asList(PANCAKES, KYIVCUTLET, DRANIKI, ICECREAM, BORSHCH);;
    MeatFarmer meatFarmer;
    VegetableFarmer vegetableFarmer;
    Manufacturer manufacturerBorshch;
    Manufacturer manufacturerDraniki;
    Manufacturer manufacturerIceCream;
    Manufacturer manufacturerKyivCutlet;
    Manufacturer manufacturerPancakes;
    Storehouse storehouse;
    Seller seller;
    Customer customer1;
    Customer customer2;
    PriceWriter visitor = new PriceWriter();
    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
    private final ByteArrayOutputStream errContent = new ByteArrayOutputStream();
    private final PrintStream originalOut = System.out;
    private final PrintStream originalErr = System.err;

    @BeforeEach
    void setUp() {
        meatFarmer = new MeatFarmer("MEAT FARMER",35, "BELARUS", 2000,
                meatIngredients, transactionInformer);
        vegetableFarmer = new VegetableFarmer("VEGETABLE FARMER", 42,"UKRAINE", 2000,
                vegetableIngredients, transactionInformer);
        manufacturerBorshch = new Manufacturer("BORSHCH MANUFACTURER", 0,
                BORSHCH, transactionInformer);
        manufacturerDraniki = new Manufacturer("DRANIKI MANUFACTURER", 1000,
                DRANIKI, transactionInformer);
        manufacturerIceCream = new Manufacturer("ICE CREAM MANUFACTURER", 1000,
                ICECREAM, transactionInformer);
        manufacturerKyivCutlet = new Manufacturer("KYIVCUTLET MANUFACTURER", 1000,
                KYIVCUTLET, transactionInformer);
        manufacturerPancakes = new Manufacturer("PANCAKES MANUFACTURER", 1000,
                PANCAKES, transactionInformer);
        storehouse = new Storehouse("STOREHOUSE", 100, transactionInformer);
        seller = new Seller("SELLER", 1000, transactionInformer);
        customer1 = new Customer("CUSTOMER-1", 400, transactionInformer);
        customer2 = new Customer("CUSTOMER-2", 1000, transactionInformer);
        Collections.addAll(participants, meatFarmer, vegetableFarmer, manufacturerBorshch, manufacturerDraniki,
                manufacturerIceCream, manufacturerKyivCutlet, manufacturerPancakes,
                storehouse, seller,  customer1, customer2);
        visitor.write(participants);
    }

    @BeforeEach
    public void setUpStreams() {
        System.setOut(new PrintStream(outContent));
        System.setErr(new PrintStream(errContent));
    }

    @AfterEach
    public void restoreStreams() {
        System.setOut(originalOut);
        System.setErr(originalErr);
    }


    void setUpChannels() {
        MeatChannel.getMeatChannel(participants, meatIngredients, transactionInformer);
        VegetableChannel.getVegetableChannel(participants, vegetableIngredients, transactionInformer);
        ReadyMealChannel.getReadyMealChannel(participants, readyMeals, transactionInformer);
        PaymentChannel.getPaymentChannel(participants, transactionInformer);

        for (Party participant : participants) {
            transactionInformer.attach(participant);
            participant.registerToTheChannel();
        }
    }

    @Test
    void storehouseBuy2Meats_storeHouseReceiveProduct() {
        // arrange

        setUpChannels();
        int expectedLeftMoney = 0;
        int expectedNumberOfProducts = 1;
        int expectedProductQuantity = 2;
        FoodEnum expectedProduct = MEAT;

        // act
        storehouse.sendRequest(MEAT, 2);
        int realLeftMoney = storehouse.getCashAccount();
        List<FoodEntity> storehouseProducts = storehouse.getProducts();
        int realNumberOfProducts = storehouseProducts.size();

        // assert
        assertEquals(expectedNumberOfProducts, realNumberOfProducts);

        // act
        FoodEntity product = storehouseProducts.get(0);
        int realProductQuantity = product.getQuantity();
        FoodEnum realProduct = product.getName();

        // assert
        assertEquals(expectedLeftMoney, realLeftMoney);
        assertEquals(expectedProduct, realProduct);
        assertEquals(expectedProductQuantity, realProductQuantity);
    }

    @Test
    void storehouseTryToBuy3Meats_noMoneyOutput() {
        // arrange
        setUpChannels();
        int expectedLeftMoney = 100;
        int expectedNumberOfProducts = 0;
        String expectedOutput = "STOREHOUSE have no money.\n";

        // act
        storehouse.sendRequest(MEAT, 3);
        int realLeftMoney = storehouse.getCashAccount();
        List<FoodEntity> storehouseProducts = storehouse.getProducts();
        int realNumberOfProducts = storehouseProducts.size();
        String realOutput = outContent.toString();

        // assert
        assertEquals(expectedLeftMoney, realLeftMoney);
        assertEquals(expectedNumberOfProducts, realNumberOfProducts);
        assertEquals(expectedOutput, realOutput);
    }

    @Test
    void customerBuyDifferentProducts_customerReceiveProducts() {
        // arrange
        setUpChannels();

        int expectedLeftMoneyCustomer = 190;
        int expectedMoneyDranikiManufacturer = 1057;
        int expectedNumberOfProducts = 3;
        List<FoodEntity> expectedProducts = Arrays.asList(new FoodEntity(MILK, 2, 0),
                new FoodEntity(ONION, 3, 0), new FoodEntity(DRANIKI, 1, 0));

        //act
        customer1.sendRequest(MILK, 2);
        customer1.sendRequest(ONION, 3);
        customer1.sendRequest(DRANIKI, 1);

        List<FoodEntity> customerProducts = customer1.getProducts();
        int realLeftMoneyCustomer = customer1.getCashAccount();
        int realNumberOfProducts = customerProducts.size();
        int realDranikiManufacturerMoney = manufacturerDraniki.getCashAccount();

        //assert
        assertEquals(expectedLeftMoneyCustomer, realLeftMoneyCustomer);
        assertEquals(expectedMoneyDranikiManufacturer, realDranikiManufacturerMoney);
        assertEquals(expectedNumberOfProducts, realNumberOfProducts);
        for (int i = 0; i < customerProducts.size(); i++) {
            FoodEnum expectedProductName = expectedProducts.get(i).getName();
            FoodEnum realProductName = customerProducts.get(i).getName();
            assertEquals(expectedProductName, realProductName);

            int expectedProductQuantity = expectedProducts.get(i).getQuantity();
            int realProductQuantity = customerProducts.get(i).getQuantity();
            assertEquals(expectedProductQuantity, realProductQuantity);
        }
    }

    @Test
    void customerOrderBorshchButManufacturerHasNoMoneyToCookSomething_noChanges() {
        // arrange
        setUpChannels();

        int expectedCustomerProductsAmount = 0;
        int expectedManufacturerProductsAmount = 0;
        int expectedLeftMoneyCustomer = 1000;
        int expectedManufacturerMoney = 0;
        int expectedMeatFarmerMoney = 2000;
        int expectedVegetableFarmerMoney = 2000;

        // act
        customer2.sendRequest(BORSHCH, 2);
        int realCustomerProductsAmount = customer2.getProducts().size();
        int realManufacturerProductsAmount = manufacturerBorshch.getProducts().size();
        int realLeftMoneyCustomer = customer2.getCashAccount();
        int realManufacturerMoney = manufacturerBorshch.getCashAccount();
        int realMeatFarmerMoney = meatFarmer.getCashAccount();
        int realVegetableFarmerMoney = vegetableFarmer.getCashAccount();

        // assert
        assertEquals(expectedCustomerProductsAmount, realCustomerProductsAmount);
        assertEquals(expectedManufacturerProductsAmount, realManufacturerProductsAmount);
        assertEquals(expectedLeftMoneyCustomer, realLeftMoneyCustomer);
        assertEquals(expectedManufacturerMoney, realManufacturerMoney);
        assertEquals(expectedMeatFarmerMoney, realMeatFarmerMoney);
        assertEquals(expectedVegetableFarmerMoney, realVegetableFarmerMoney);

    }

    @Test
    void reportMilkInStorehouseAndCustomer_rightOutput() {
        // arrange
        setUpChannels();
        FoodEnum expectedProductName = MILK;
        int expectedNumberOfProducts = 1;


        // act
        storehouse.sendRequest(MILK, 1);
        List<FoodEntity> realStorehouseProducts = storehouse.getProducts();
        int realNumberOfProductsStorehouse = realStorehouseProducts.size();

        // assert
        assertEquals(expectedNumberOfProducts, realNumberOfProductsStorehouse);

        // act
        FoodEntity realProductStorehouse = realStorehouseProducts.get(0);
        String expectedMilkReportInStorehouse = "Product " + expectedProductName +
                " cannot be transported until it is not packed. It will be packed in transport packaging.\n" +
                realProductStorehouse + " with name " + expectedProductName + " was created by " + meatFarmer + "\n" +
                realProductStorehouse + " with name " + expectedProductName + " was put to warm fridge in " + storehouse + "\n";

        FoodEnum realProductNameStorehouse = realProductStorehouse.getName();
        realProductStorehouse.writeAllActions();
        String realMilkReportInStorehouse = outContent.toString();

        // assert
        assertEquals(expectedProductName, realProductNameStorehouse);
        assertEquals(expectedMilkReportInStorehouse, realMilkReportInStorehouse);

        // act
        customer1.sendRequest(MILK, 1);
        List<FoodEntity> realCustomerProducts = customer1.getProducts();
        int realNumberOfProductsCustomer = realCustomerProducts.size();

        // assert
        assertEquals(expectedNumberOfProducts, realNumberOfProductsCustomer);

        //act
        FoodEntity realProductCustomer = realCustomerProducts.get(0);
        String expectedMilkReportInCustomer = expectedMilkReportInStorehouse +
                "Product MILK cannot be presented to customer until it is not finally packed."
                + " It will be packed in final packaging.\n" + realProductStorehouse + " with name " +
                expectedProductName + " was created by " + meatFarmer + "\n" + realProductStorehouse +
                " with name " + expectedProductName + " was put to warm fridge in " + storehouse + "\n" +
                realProductCustomer + " with name " + expectedProductName + " come to " + customer1 + "\n";
        realProductCustomer.writeAllActions();
        String realMilkReportInCustomer = outContent.toString();

        // assert
        assertEquals(realProductStorehouse, realProductCustomer);
        assertEquals(expectedMilkReportInCustomer, realMilkReportInCustomer);
    }

    @Test
    void generatePartyReportForPancakesManufacturer_rightOutput() {
        // arrange
        setUpChannels();
        restoreStreams();
        String[] expectedReportOutput = {"PRODUCT: MILK || PRODUCTS SENDER: MEAT FARMER || QUANTITY 1\n" ,
                "PRODUCT: SUGAR || PRODUCTS SENDER: VEGETABLE FARMER || QUANTITY 1\n" ,
                "PRODUCT: FLOUR || PRODUCTS SENDER: VEGETABLE FARMER || QUANTITY 1\n"};
        FoodEnum expectedProductName = PANCAKES;

        // act
        customer2.sendRequest(PANCAKES, 1);
        List<FoodEntity> realCustomerProducts = customer2.getProducts();
        FoodEntity realProductCustomer = realCustomerProducts.get(0);
        FoodEnum realProductName = realProductCustomer.getName();
        setUpStreams();
        Main.generatePartiesReport(manufacturerPancakes);
        String realReportOutput = outContent.toString();

        //assert
        assertEquals(expectedProductName, realProductName);
        assertTrue(realReportOutput.contains(expectedReportOutput[0]));
        assertTrue(realReportOutput.contains(expectedReportOutput[1]));
        assertTrue(realReportOutput.contains(expectedReportOutput[2]));
    }


    @Test
        // TEST IS NOT PASSED
        // VEGETABLE FARMER CANNOT ORDER READY MEAL WITH INGREDIENTS WHICH HE GROWS
        // expectedNumberOfProductsFarmer = 1
        // realNumberOfProductsFarmer = 0
    void vegetableFarmerOrdersIceCream_getNothing() {
        // arrange
        setUpChannels();
        int expectedManufacturerMoney = 1060;
        int expectedFarmerMoney = 940;
        int expectedNumberOfProductsFarmer = 1;
        FoodEnum expectedFarmerProductName = ICECREAM;

        // act
        vegetableFarmer.sendRequest(ICECREAM, 3);
        List<FoodEntity> farmerProducts = vegetableFarmer.getProducts();
        int realNumberOfProductsFarmer = farmerProducts.size();
        int realFarmerMoney = vegetableFarmer.getCashAccount();
        int realManufacturerMoney = manufacturerIceCream.getCashAccount();

        //assert
        assertEquals(expectedNumberOfProductsFarmer, realNumberOfProductsFarmer);

        // act
        FoodEntity farmerProduct = farmerProducts.get(0);
        FoodEnum realFramerProductName = farmerProduct.getName();

        //assert
        assertEquals(expectedFarmerMoney, realFarmerMoney);
        assertEquals(expectedManufacturerMoney, realManufacturerMoney);
        assertEquals(expectedFarmerProductName, realFramerProductName);
    }


    @Test
        // TEST FAILED
        // CUSTOMER DON'T GET BORSHCH
        // expectedNumberOfCustomerProducts = 1
        // realProductName = 0
    void customerTryToBuyBorshch_getNothing() {
        // arrange
        setUpChannels();
        int expectedNumberOfCustomerProducts = 1;
        FoodEnum expectedProductName = BORSHCH;

        // act
        customer1.sendRequest(BORSHCH, 1);
        List<FoodEntity> realCustomerProducts = customer1.getProducts();
        int realNumberOfCustomer1Products = realCustomerProducts.size();

        // assert
        assertEquals(expectedNumberOfCustomerProducts, realNumberOfCustomer1Products);

        // act
        FoodEntity realCustomerProduct = realCustomerProducts.get(0);
        FoodEnum realProductName = realCustomerProduct.getName();

        // assert
        assertEquals(expectedProductName, realProductName);
    }
}