package cz.cvut.fel.omo.foodChain.product;

import cz.cvut.fel.omo.foodChain.chainParticipants.Customer;
import cz.cvut.fel.omo.foodChain.operations.TransactionInformer;
import cz.cvut.fel.omo.foodChain.product.FoodEntity;
import cz.cvut.fel.omo.foodChain.states.State;
import cz.cvut.fel.omo.foodChain.states.TemporaryPackedState;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static cz.cvut.fel.omo.foodChain.product.FoodEnum.POTATO;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;


public class FoodEntityTest {

    private FoodEntity foodEntity;

    @BeforeEach
    public void setUp() {
        foodEntity = new FoodEntity(POTATO, 1, 1);
    }

    @Test
    void addAction_addAction1andAction2_action1action2() {
        // arrange
        List<String> expectedActions = new ArrayList<>();
        Collections.addAll(expectedActions, "Action 1", "Action 2");
        // act
        foodEntity.addAction("Action 1");
        foodEntity.addAction("Action 2");
        // assert
        assertEquals(expectedActions, foodEntity.getDoneActions());
    }

    @Test
    void transport_tryToTemporaryPackForCustomer_notPossibleAnotherState() {
        // arrange
        State expectedState = new TemporaryPackedState(this.foodEntity);
        // act
        foodEntity.transport(new Customer("Lukas", 100, new TransactionInformer()));
        State actualState = foodEntity.getState();
        // assert
        assertNotEquals(expectedState, actualState);
    }
}