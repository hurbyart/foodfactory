package cz.cvut.fel.omo.foodChain.channels;

import cz.cvut.fel.omo.foodChain.channels.PaymentChannel;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;


public class PaymentChannelTest {

    /**
        checking correct working of singleton pattern
        trustedParticipants and transactionInformer are null because it is not important in this test
     */
    @Test
    void getPaymentChannel_emptyTrustedParticipantsAndTransactionInformer_thisChannel() {
        //act
        PaymentChannel initialChannel = PaymentChannel.getPaymentChannel(null, null);
        PaymentChannel gottenChannel = PaymentChannel.getPaymentChannel();
        //assert
        assertEquals(initialChannel, gottenChannel);

    }


}

